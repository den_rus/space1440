import re
import subprocess
import time
from multiprocessing import Process

import mock
import requests
import yaml

import space1440


def func_side_effect(*args):
    for arg in args:
        space1440.logger.info(str(arg))
    return args


space1440.sendcmd = mock.Mock(side_effect=func_side_effect)

with open("space1440.yaml", "r", encoding="utf-8") as stream:
    data = yaml.safe_load(stream)
    MONITOR_INTERVAL = data["MONITOR_INTERVAL"]
    MONITOR_ENDTIME = time.time() + data["MONITOR_ENDTIME"]
    LOG_FILENAME = data["LOG_FILENAME"]


def func_start_unicorn():
    subprocess.call(["python3", "space1440.py"])


def test_sendcmd():
    out = space1440.sendcmd(b"12345", "127.0.0.1", "8000")
    assert out[0] == b"12345", "COMMAND unexpected!"
    assert out[1] == "127.0.0.1", "IP unexpected!"
    assert out[2] == "8000", "PORT unexpected!"


def test_monitor():
    pro = Process(target=func_start_unicorn)
    pro.start()
    time.sleep(3)
    with open(LOG_FILENAME, "r", encoding="utf-8") as file:
        len1 = len(file.readlines())
    time.sleep(MONITOR_INTERVAL + (0.1 * MONITOR_INTERVAL))
    pro.terminate()
    with open("/tmp/monitor.log", "r", encoding="utf-8") as file:
        len2 = len(file.readlines())
    assert len2 > len1, f"Log file {LOG_FILENAME} not updated!"


def test_get_state():
    pro = Process(target=func_start_unicorn)
    pro.start()
    time.sleep(3)
    api_url = "http://127.0.0.1:8000/state"
    response = requests.get(api_url, verify=False)
    space1440.logger.info(response.status_code)
    space1440.logger.info(response.text)
    pro.terminate()
    assert response.status_code == 200, "Unexpected code!"
    assert re.findall(
        r'{"1":{"Time":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Voltage":"Any answer from device","Current":"Any answer from device"},'
        + r'"2":{"Time":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Voltage":"Any answer from device","Current":"Any answer from device"},'
        + r'"3":{"Time":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Voltage":"Any answer from device","Current":"Any answer from device"},'
        + r'"4":{"Time":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Voltage":"Any answer from device","Current":"Any answer from device"}}',
        response.text,
    ), "Unexpected answer!"


def test_on_channel():
    p = Process(target=func_start_unicorn)
    p.start()
    time.sleep(3)
    api_url = "http://127.0.0.1:8000/on"
    request_data = {"channel": "1", "volt": "220.0", "amp": "1.1"}
    response = requests.post(api_url, json=request_data, verify=False)
    space1440.logger.info(response.status_code)
    space1440.logger.info(response.text)
    p.terminate()
    assert response.status_code == 200, "Unexpected code!"
    assert re.findall(
        r'{"Time:":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Channel:":1,"Current state:":"Any answer from device"}',
        response.text,
    ), "Unexpected response.text!"


def test_off_channel():
    p = Process(target=func_start_unicorn)
    p.start()
    time.sleep(3)
    api_url = "http://127.0.0.1:8000/off"
    request_data = {"channel": "2"}
    response = requests.post(api_url, json=request_data, verify=False)
    space1440.logger.info(response.status_code)
    space1440.logger.info(response.text)
    p.terminate()
    assert response.status_code == 200, "Unexpected code!"
    assert re.findall(
        r'{"Time:":"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d","Channel:":2,"Current state:":"Any answer from device"}',
        response.text,
    ), "Unexpected response.text!"
