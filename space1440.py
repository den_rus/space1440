#!/usr/bin/python3
import asyncio
import logging
import socket
import sys
from dataclasses import dataclass
from datetime import datetime

import uvicorn
import yaml
from fastapi import FastAPI
from pydantic import BaseModel  # pylint: disable=no-name-in-module

logger = logging.getLogger(__name__)

space1440 = FastAPI()

with open("space1440.yaml", "r", encoding="utf-8") as stream:
    data = yaml.safe_load(stream)
    IP = data["IP"]  # IP address of the device to connect
    PORT = data["PORT"]  # Port of the device to connect
    SOCKET_TIMEOUT = data["SOCKET_TIMEOUT"]  # Socket timeout during connection to the device


@dataclass
class ChannelOFF:
    channel: int


class ChannelON(BaseModel):
    channel: int
    volt: float
    amp: float


def sendcmd(cmd, ipa=IP, port=PORT):
    try:  # MOC just for tests
        # Send command and read answer
        sock = socket.create_connection((ipa, port))
        sock.settimeout(SOCKET_TIMEOUT)
        cmd = bytes(cmd + "\n", encoding="utf-8")
        sock.sendall(cmd)
        logger.debug("Command was sent to the socket.")
        # Read socket answer
        logger.debug("Timeout to wait answer from socket is\n%s", SOCKET_TIMEOUT)
        try:
            buff_size = 4096  # 4 KB
            answer = bytearray()
            while True:
                part = sock.recv(buff_size)
                answer += part
                if len(part) < buff_size:
                    break
        except socket.timeout as erratum:
            logger.debug("ERROR: Communication with socket\n%s", erratum)
        sock.close()
        return answer
    except ConnectionRefusedError:  # MOC just for tests
        return "Any answer from device"  # MOC just for tests


async def start_monitor():
    code = "import monitor"
    await asyncio.create_subprocess_exec(
        sys.executable, "-c", code, stdout=asyncio.subprocess.PIPE
    )


@space1440.post("/on")
async def on_channel(channel: ChannelON):
    """
    ON channel of power supply with parameters:
        - Channel number
        - Voltage
        - Current
    """

    sendcmd(f":SOURce{channel.channel}:VOLTage {channel.volt}")
    sendcmd(f":SOURce{channel.channel}:CURRent {channel.amp}")
    sendcmd(f":OUTPut{channel.channel}:STATe ON")
    answer = sendcmd(f":OUTPut{channel.channel}:STATe?")
    ts_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    return {"Time:": ts_now, "Channel:": channel.channel, "Current state:": answer}


@space1440.post("/off")
async def off_channel(channel: ChannelOFF):
    """
    OFF channel of power supply with parameter:
        - Channel number
    """
    sendcmd(f":OUTPut{channel.channel}:STATe OFF")
    answer = sendcmd(f":OUTPut{channel.channel}:STATe?")
    ts_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    return {"Time:": ts_now, "Channel:": channel.channel, "Current state:": answer}


@space1440.get("/state")
async def get_status():
    """
    Current state of all channels of power supply
        - Time of measurement
        - Voltage
        - Current
    """
    output = {}
    for chn in ["1", "2", "3", "4"]:
        output[chn] = {}

        ts_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        output[chn]["Time"] = ts_now

        answer = sendcmd(f":MEASure{chn}:VOLTage?")
        logger.info("%s Channel:%s Voltage:%s\n", ts_now, chn, answer)
        output[chn]["Voltage"] = f"{answer}"

        answer = sendcmd(f":MEASure{chn}:CURRent?")
        logger.info("%s Channel:%s Current:%s\n", ts_now, chn, answer)
        output[chn]["Current"] = f"{answer}"

    return output


def main():
    asyncio.run(start_monitor())
    uvicorn.run(space1440, host="0.0.0.0", port=8000)


if __name__ == "__main__":
    main()
