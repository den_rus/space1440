"""
Monitor power supply: voltage, current, power by every channel
"""
import time
from datetime import datetime

import yaml

from space1440 import sendcmd

with open("space1440.yaml", "r", encoding="utf-8") as stream:
    data = yaml.safe_load(stream)
    MONITOR_INTERVAL = data["MONITOR_INTERVAL"]
    MONITOR_ENDTIME = time.time() + data["MONITOR_ENDTIME"]
    LOG_FILENAME = data["LOG_FILENAME"]

with open(LOG_FILENAME, "w", encoding="utf-8"):
    pass
while True:
    for chn in ["1", "2", "3", "4"]:
        answer = sendcmd(f":MEASure{chn}:ALL?")
        ts_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open(LOG_FILENAME, "a", encoding="utf-8") as the_file:
            the_file.write(f"{ts_now} Channel:{chn} {answer}\n")
    time.sleep(MONITOR_INTERVAL)
    if time.time() > MONITOR_ENDTIME:
        break
