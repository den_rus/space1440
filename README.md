# space1440

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/den_rus/space1440.git
git branch -M main
git push -uf origin main
```
## Name
Driver for programmable DC Power Supply
GPP-1326/GPP-2323/GPP-3323/GPP-4323

## Description
Test task for Space1440 company.
See task.txt for details.

## Requirements
- Any usual Linux system (tested on "Fedora Linux" VERSION="35 Workstation Edition)"
- Python 3.10.0 with installed requirements (see file pyproject.toml)
## Installation
- No needs if all requirements are OK
- Requirements described in pyproject.toml file in section [tool.poetry.dependencies] and could be install with poetry
- Python requirements could be installed manual by commands:
```
$ python -m pip install fastapi
$ python -m pip install pytest
$ python -m pip install mock
$ python -m pip install uvicorn[standard]
$ python -m pip install pydantic
$ python -m pip install asyncio
```
## Usage
- To start tests - from directory of the project launch:
```
$ pytest
or
$ pytest --log-cli-level=debug # with debug log messages
```
- To start application - from directory of the project launch:
```
$ ./space1440.py
```
- Monitor start in background and collect data to the /tmp/monitor.log file
- Open up a browser and go to http://127.0.0.1:8000/state for getting current state of the device
## Configuration
- Configuration present in space1440.yaml file
```
IP: "127.0.0.1" # IP address of the device to connect
PORT: 7777 # Port of the device to connect
MONITOR_INTERVAL: 3 # Interval in seconds between sending status requests to the device
MONITOR_ENDTIME: 60 # Time in seconds after that monitor stopped
SOCKET_TIMEOUT: 3 # Socket timeout during connection to the device
LOG_FILENAME: "/tmp/monitor.log" # File where monitor write the log data
```
## Support
Denis Drozdov mailto: denis.dv@gmail.com
